
INSERT INTO oauth_client_details(CLIENT_ID, RESOURCE_IDS, CLIENT_SECRET, SCOPE, AUTHORIZED_GRANT_TYPES, AUTHORITIES, ACCESS_TOKEN_VALIDITY, REFRESH_TOKEN_VALIDITY) VALUES ('spring-security-oauth2-read-client', 'resource-server-rest-api','$2a$04$3IY6ZOrlvzRlh3Op8GdEMem5JtKl.pOcRKLotqcZKlN14fYddYGPa','read', 'password,authorization_code,refresh_token,implicit', 'USER', 10800, 2592000);
INSERT INTO oauth_client_details(CLIENT_ID, RESOURCE_IDS, CLIENT_SECRET, SCOPE, AUTHORIZED_GRANT_TYPES, AUTHORITIES, ACCESS_TOKEN_VALIDITY, REFRESH_TOKEN_VALIDITY) VALUES ('spring-security-oauth2-read-write-client', 'resource-server-rest-api','$2a$04$3IY6ZOrlvzRlh3Op8GdEMem5JtKl.pOcRKLotqcZKlN14fYddYGPa','read,write', 'password,authorization_code,refresh_token,implicit', 'USER', 10800, 2592000);


INSERT INTO scr_user VALUES(1,1,1,1,1,'$2a$12$xTnx9IuV4Kb2OZav540BReKnqgpIYYgHce4ZJb2QTxTQ6j7H5NLlS','admin');
INSERT INTO scr_user VALUES(2,1,1,1,1,'$2a$12$kRstkVGM88bvzGqxcWDX5eVDvT0iy40YPAKMRiKX9D2jIsXmKXOAK','user');


insert into scr_role values(1,'ROLE_USER');
insert into scr_role values(2,'ROLE_ADMIN');
insert into scr_role values(3,'ROLE_APIUSER');
insert into scr_role values(4,'ROLE_DBA');
insert into scr_role values(5,'ROLE_SELLER');
insert into scr_role values(6,'ROLE_BUYER');

insert into scr_module values(1,'MST_BANK_VIEW');
insert into scr_module values(2,'MST_BANK_UPDATE');
insert into scr_module values(3,'MST_BANK_DELETE');

insert into scr_privilege values(1,'READ_PRIVILEGE',1);
insert into scr_privilege values(2,'WRITE_PRIVILEGE',2);
insert into scr_privilege values(3,'DELETE_PRIVILEGE',3);

INSERT INTO scr_user_role VALUES (1,2,1);
INSERT INTO scr_user_role VALUES (2,1,1);
INSERT INTO scr_user_role VALUES (3,1,2);

insert into scr_role_privilege values(2,1);
insert into scr_role_privilege values(2,2);
insert into scr_role_privilege values(2,3);
insert into scr_role_privilege values(1,1);


INSERT INTO scr_category VALUES(1002,'Grocery');
INSERT INTO scr_category VALUES(1003,'Electronics');
INSERT INTO scr_category VALUES(1001,'Books');
