/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gudangdunia.oauth.util;

import java.util.Collection;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author imamsolikhin
 */
public class TokenUtil {

  public static String getSessionValue(HttpServletRequest request) {
    String sessionValue = null;
    Cookie[] cookies = request.getCookies();
    for (Cookie cookie : cookies) {
      if (cookie.getName().equals("JSESSIONID")) {
        sessionValue = cookie.getValue();
      }
    }
    return sessionValue;
  }

  public static boolean hasAdminRole(Collection<? extends GrantedAuthority> authorities) {
    for (GrantedAuthority grantedAuthority : authorities) {
      if (grantedAuthority.getAuthority().equals("ROLE_USER")) {
        return false;
      } else if (grantedAuthority.getAuthority().equals("ROLE_ADMIN")) {
        return true;
      }
    }
    return false;
  }
}
