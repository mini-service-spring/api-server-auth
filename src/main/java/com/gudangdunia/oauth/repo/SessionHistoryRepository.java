package com.gudangdunia.oauth.repo;

import com.gudangdunia.oauth.model.SessionHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SessionHistoryRepository extends JpaRepository<SessionHistory, Long> {
}
