package com.gudangdunia.oauth.repo;

import com.gudangdunia.oauth.model.Privilage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PrivilegeRepository extends JpaRepository<Privilage, Long> {
}
