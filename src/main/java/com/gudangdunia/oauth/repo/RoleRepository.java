package com.gudangdunia.oauth.repo;

import com.gudangdunia.oauth.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
