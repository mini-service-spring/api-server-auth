package com.gudangdunia.oauth.repo;

import com.gudangdunia.oauth.model.FailedLogin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FailedLoginRepository extends JpaRepository<FailedLogin, Long> {
}
