package com.gudangdunia.oauth.repo;

import com.gudangdunia.oauth.model.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserProfileRepository extends JpaRepository<UserProfile, Long> {
}
