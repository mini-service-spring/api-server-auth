package com.gudangdunia.oauth.api;

import com.gudangdunia.oauth.model.UserProfile;
import com.gudangdunia.oauth.repo.UserProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserProfileController {

  private UserProfileRepository userProfileRepository;

  @Autowired
  public UserProfileController(UserProfileRepository userProfileRepository) {
    this.userProfileRepository = userProfileRepository;
  }

  @PostMapping(value = "/userprofile/create")
  public UserProfile createNewUserProfile(@RequestBody UserProfile UserProfile) {
    return userProfileRepository.saveAndFlush(UserProfile);
  }

  @GetMapping(value = "/userprofiles")
  public List<UserProfile> findAll() {
    return userProfileRepository.findAll();
  }

}
