package com.gudangdunia.oauth.model;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Data
@Table(name = "scr_user")
public class User implements Serializable {

  @Id
  @Column(name = "Code")
  private String code;

  @Column(name = "username")
  private String username;

  @Column(name = "active")
  private Boolean active = false;

  @Column(name = "credentialsNonExpired")
  private Boolean credentialsNonExpired = false;

  @Column(name = "accountNonLocked")
  private Boolean accountNonLocked = false;

  @Column(name = "AccountNonExpired")
  private Boolean accountNonExpired = false;

  @Column(name = "password", nullable = false)
  private String password;

  @OneToOne(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  private UserProfile userProfile;

  @ManyToMany
  @JoinTable(
          name = "scr_user_role",
          joinColumns = @JoinColumn(name = "userCode", referencedColumnName = "Code"),
          inverseJoinColumns = @JoinColumn(name = "roleCode", referencedColumnName = "Code")
  )
  private Collection<Role> roles;

  public User() {

  }

  public User(String username, String password, Boolean active, UserProfile userProfile) {
    this.username = username;
    this.password = password;
    this.active = active;
    this.userProfile = userProfile;
  }
}
