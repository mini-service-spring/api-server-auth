package com.gudangdunia.oauth.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Data
@Table(name = "scr_role")
public class Role implements Serializable {

  @Id
  @Column(name = "Code")
  private String code;

  @Column(name = "name", nullable = false)
  private String name;

  @ManyToMany(mappedBy = "roles")
  @JsonIgnore
  private Collection<User> users;

  @ManyToMany
  @JoinTable(
          name = "scr_role_privilege",
          joinColumns = @JoinColumn(name = "roleCode", referencedColumnName = "Code"),
          inverseJoinColumns = @JoinColumn(name = "privilegeCode", referencedColumnName = "Code")
  )
  private Collection<Privilage> privileges;

  public Role() {
  }

  public Role(String name) {
    this.name = name;
  }

  public Role(String name, Collection<User> users, Collection<Privilage> privileges) {
    this.name = name;
    this.users = users;
    this.privileges = privileges;
  }
}
