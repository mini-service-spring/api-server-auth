package com.gudangdunia.oauth.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Data
@Table(name = "scr_user_profile")
public class UserProfile implements Serializable {

  @Id
  @Column(name = "Code")
  private String code;

  @Column(name = "FirstName", nullable = false)
  private String firstName;

  @Column(name = "LastName", nullable = false)
  private String lastName;

  @Column(name = "Email", nullable = false)
  private String email;

  @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JsonIgnore
  private User user;
}
