package com.gudangdunia.oauth.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Data
@Table(name = "scr_module")
public class Module implements Serializable {

  @Id
  @Column(name = "Code")
  private String code;

  @Column(name = "Name", nullable = false)
  private String name;

  @OneToOne(mappedBy = "module", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  private Privilage privilage;
}
