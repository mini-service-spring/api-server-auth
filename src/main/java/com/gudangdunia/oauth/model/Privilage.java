package com.gudangdunia.oauth.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
@Data
@Table(name = "scr_privilege")
public class Privilage implements Serializable {

  @Id
  @Column(name = "Code")
  private String code;

  private String name;

  @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JsonIgnore
  private Module module;

  @ManyToMany(mappedBy = "privileges")
  @JsonIgnore
  private Collection<Role> roles;

  public Privilage() {
  }

  public Privilage(String name) {
    this.name = name;
  }

  public Privilage(String name, Collection<Role> roles) {
    this.name = name;
    this.roles = roles;
  }
}
