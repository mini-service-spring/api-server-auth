package com.gudangdunia.oauth.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Data
@Table(name = "scr_user_role")
public class UserRole implements Serializable {

  @Id
  @Column(name = "Code")
  private String code;

  @Column(name = "userCode")
  private String userCode;

  @Column(name = "roleCode")
  private String roleCode;
}
