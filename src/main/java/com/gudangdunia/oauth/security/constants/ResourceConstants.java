package com.gudangdunia.oauth.security.constants;

public class ResourceConstants {

  public static final String RESOURCE_SERVER_REST_API = "resource-server-api";

  private ResourceConstants() {

  }
}
