package com.gudangdunia.oauth.security;

import com.gudangdunia.oauth.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class MyUserDetails implements UserDetails {

  private final User user;

  private final Collection<? extends GrantedAuthority> roles;

  MyUserDetails(User user, Collection<? extends GrantedAuthority> roles) {
    this.user = user;
    this.roles = roles;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return roles;
  }

  @Override
  public String getPassword() {
    return user.getPassword();
  }

  @Override
  public String getUsername() {
    return user.getUsername();
  }

  @Override
  public boolean isAccountNonExpired() {
    return user.getAccountNonExpired();
  }

  @Override
  public boolean isAccountNonLocked() {
    return user.getAccountNonLocked();
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return user.getCredentialsNonExpired();
  }

  @Override
  public boolean isEnabled() {
    return user.getActive();
  }

}
